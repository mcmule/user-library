


                 !!!!!!!!!!!!!!!!!!!!!
                     MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!

  use functions

  implicit none

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

  integer, parameter :: nr_q = 3
  integer, parameter :: nr_bins = 140
  real, parameter :: &
     min_val(nr_q) = (/ 20., 0.5, -14000. /)
  real, parameter :: &
     max_val(nr_q) = (/ 160., 4., 0. /)
  real(kind=prec) :: vcut
  integer hardcut
!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

    !! ============================================== !!
    !! DO NOT EVEN THINK ABOUT CHANGING ANYTHING HERE !!
    !! ============================================== !!

  logical ::  pass_cut(nr_q)
  character (len = 6), dimension(nr_q) :: names
  character(len=10) :: filenamesuffix


!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!==!

            !! ----------------------------------------- !!
            !!     There are two versions of binning     !!
            !!     One for computing   d \sigma/ d Q     !!
            !!     One for computing  Q d \sigma/ d Q    !!
            !!  choose by setting the variable bin_kind  !!
            !! ----------------------------------------- !!
  integer :: bin_kind = 0       !!  0 for d \sig/dQ; +1 for Q d \sig/dQ;


  contains




  SUBROUTINE FIX_MU

  !! ==== Specify the scale mu AND musq==mu**2 ==== !!

  mu = Me

  musq = mu**2

  END SUBROUTINE FIX_MU



  SUBROUTINE USERINIT
  ! This is called without arguments once as soon as McMule
  ! starts and has read all other configuration, i.e. you can
  ! access which_piece and flavour. Use this to read any
  ! further information from the user (like cut configuration
  ! etc). You do not have to print the hashes - this is
  ! already taken care of - but you are very much invited to
  ! include information of what it is you are doing
  !
  ! If you are using the cut channel of the menu, you may need to set
  ! the filenamesuffix variable which is appended to the name of the
  ! VEGAS file.

  ! Example for reading a cut:
  ! integer cut
  ! read*,cut
  ! write(filenamesuffix,'(I2)') cut
  integer cut ! This shall be between 0 and 9 (incl)
  read*, cut
  vcut = (1+cut)/10.
  write(filenamesuffix,'(I1)') cut

  hardcut = 8

  print*, "This calculates the Moller scattering as implemented"
  print*, "in [hep-ph/0504191] with vcut = ",vcut
  

  END SUBROUTINE




  FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)

  real (kind=prec), intent(in) :: q1(4),q2(4),q3(4),q4(4), q5(4),q6(4),q7(4)
  real (kind=prec) :: gah(4), gas(4), tt, ss, theta, slam, v, vmax
  real (kind=prec) :: q1rest(4),q2rest(4),q3rest(4),q4rest(4)
  real (kind=prec) :: quant(nr_q)

!  pol1 = (/ 0._prec, 0._prec, 0._prec, 0._prec /)

  !! ==== keep the line below in any case ==== !!

  pass_cut = .true.
  call fix_mu

  q1rest = boost_rf(q2, q1)
  q2rest = boost_rf(q2, q2)
  q3rest = boost_rf(q2, q3)
  q4rest = boost_rf(q2, q4)

  ss = sq(q1+q2)
  tt = sq(q1-q3)

  slam = sq(q1 - q3 + q2)
  v = slam - me**2
  vmax = ss + tt
  theta =acos(1 + 2*tt/ss)

  if(theta < pi / 180 * 20 .or. theta > pi / 180 * 160) then
    pass_cut = .false.
  end if

  if(v > vcut * vmax) pass_cut = .false.

  names(1) = "thcms"
  quant(1) = 180 / pi * theta
  names(2) = "thlab"
  quant(2) = acos(cos_th(q1rest,q3rest)) * 180 / pi
  names(3) = "t13"
  quant(3) = tt
  
  
  END FUNCTION QUANT






                 !!!!!!!!!!!!!!!!!!!!!!!
                     END MODULE  USER
                 !!!!!!!!!!!!!!!!!!!!!!!



