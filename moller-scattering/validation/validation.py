# vim: foldmethod=marker
## Init{{{
# We begin by loading pyMule
from pymule import *

# and point it to the correct folder
setup(folder='ee2ee0504191/out.tar.bz2', obs='8')
##########################################################################}}}
## Load data{{{
# Load the LO with the correct factors of alpha and conv
lo = scaleset(mergefks(sigma('ee2ee0')), alpha**2*conv)


# Load the NLO, either with plot or without
#nlo = scaleset(mergefks(sigma('ee2eeF'), sigma('ee2eeR')), alpha**3*conv)
figxi, nlo = mergefkswithplot(
    [[sigma('ee2eeF')], [sigma('ee2eeR')]],
    scale=alpha**3*conv
)
figxi.savefig("xicut.pdf")

##########################################################################}}}
## Make plots{{{
### Simple plots{{{
# Let's start with a quick LO plot. Because we already have a figure
# open (the $\xi_c$-study), we first need a new one
figure()
errorband(lo['thcms'])
# Next, we look at the tK factor
printnumber(dividenumbers(nlo['value'], lo['value']))
# Now we can make a K-plot
figK, (ax1, ax2) = kplot(
    {'lo': mergebins(lo['thcms'], 4), 'nlo': mergebins(nlo['thcms'], 4)},
    labelx='$\\theta_{CMS}$',
    labelsigma='$\\D\\sigma/\\D\\theta_{CMS}\ / \ {\\rm \upmu b}$',
    legend={
        'lo': '$\\sigma^{(0)}$',
        'nlo': '$\\sigma^{(1)}$'
    },
    legendopts={'what': 'u', 'loc': 'upper center', 'ncol': 2}
)
ax1.set_yscale('log')
figK.savefig("kfac.pdf")
###########################################################}}}
### bin-wise $\chi^2${{{
# Let's also look at the bin-wise $\chi^2$ of the FKS merge. For this
# we have to do the FKS merge manually. Let's begin by loading the
# relevant data.
dataF = sigma('ee2eeF')
dataR = sigma('ee2eeR')
# Next, we need to know the FKS parameters used. These are the keys of
# the sigma result. We will assume they are the same for R and F,
# otherwise we would have to use an intersection routine such as
# pymule.loader.multiintersect
xicuts = dataF.keys()
# Now we can add the thcms plot of ee2eeF and ee2eeF for each $\xi_c$
# and merge the results.
pl, chi = mergeplots([
    addplots(
        sigma('ee2eeF')[xic]['thcms'],
        sigma('ee2eeR')[xic]['thcms']
    ) for xic in xicuts],
    True
)
figure()
scatter(chi[:, 0], chi[:, 1])
xlabel('$\\theta_{CMS}$')
ylabel('$\\chi^2$')
###########################################################}}}
##########################################################################}}}
