FOLDERS=mu-e-scattering/muone \
        mu-e-scattering/muone-legacy \
        l-p-scattering/muse \
        l-p-scattering/muse-legacy l-p-scattering/mesa-legacy \
        radiative-lepton-decay/babar/example \
        michel-decay/validation michel-decay/demo-observable michel-decay/f-and-g

.DEFAULT_GOAL=all
.PHONY:check clean ci
all:
	for i in $(FOLDERS) ; do make -C $$i all ; done
check:
	for i in $(FOLDERS) ; do make -C $$i check ; done
clean:
	for i in $(FOLDERS) ; do make -C $$i clean ; done
ci:
	rm -rf ci
	for i in $(FOLDERS) ; do make -C $$i ci ; done
