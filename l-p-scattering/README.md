# lepton proton scatter $`\mu p\to \mu p`$

References: [[tbd]](https://arxiv.org/abs/20??.?????)

Configurations implemented
 * MESA $`ep\to ep`$, P2 experiment $`p_\text{in} = 155\,{\rm MeV}`$ with $`E_e > 45\,{\rm MeV}`$
   and $`25^\circ < \theta < 45^\circ`$
 * MUSE, $`ep\to ep`$ at $`p_\text{in} = 115\,{\rm MeV}`$
   [[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/muse/muse.ipynb?job=run)
