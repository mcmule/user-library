# vim: foldmethod=marker
from pymule import *
from pymule.plot import twopanel, threepanel
from pymule.colours import orderscheme
import pymule.plot

setup(folder='mp2mp_muse_paper/out.tar.bz2')

## Load{{{
lo = scaleset(mergefks(sigma('mp2mp0')), alpha**2*conv)


nloNoVP = scaleset(mergefks(
    sigma('mp2mpF'),
    sigma('mp2mpR15'), sigma('mp2mpR35')
), alpha**3*conv)

nnloNoVP = scaleset(mergefks(
    sigma('mp2mpFF'),
    sigma('mp2mpRF15'), sigma('mp2mpRF35'),
    sigma('mp2mpRR1516'), sigma('mp2mpRR3536'),
), alpha**4*conv)


nlo = scaleset(mergefks(
    sigma('mp2mpF'),
    sigma('mp2mpR15'), sigma('mp2mpR35'),
    anyxiVP=sigma('mp2mpA')
), alpha**3*conv)

nnlo = scaleset(mergefks(
    sigma('mp2mpFF'), sigma('mp2mpAF'),
    sigma('mp2mpRF15'), sigma('mp2mpRF35'),
    sigma('mp2mpAR15'), sigma('mp2mpAR35'),
    sigma('mp2mpRR1516'), sigma('mp2mpRR3536'),
    anyxiF=sigma('mp2mpAA'),
    anyxiNF=sigma('mp2mpNF')
), alpha**4*conv)
#####################################################################}}}
## Make pictures{{{
### $\theta_e$ w/o & w/ VP{{{
fig, (ax1, ax2, ax3) = kplot(
    {
        'lo':   lo['thetal'],
        'nlo':  nlo['thetal'],
        'nnlo': nnlo['thetal'],

        'nlo2': nloNoVP['thetal'],
        'nnlo2': nnloNoVP['thetal']
    },
    labelx="$\\theta_e\,/\,{\\rm deg}$",
    labelsigma="$\\D\\sigma/\\D\\theta_\\mu\ /\ {\\rm\\upmu b}$",
    legend={
        'lo': '$\\sigma^{(0)}$',
        'nlo': '$\\sigma^{(1)}$',
        'nnlo': '$\\sigma^{(2)}$'
    },
    legendopts={'what': 'u', 'loc': 'upper right'}
)
ylim(0.99995-1, 1.0002-1)
pymule.plot.mpl_axes_aligner.yaxes(ax2, ax3, y1=0.)
fig.savefig('plots/thetamu.pdf')


###########################################################}}}
### bands{{{
def sanitise(x):
    x[x[:, 1] == 0, 1] = -10
    return x


bands = ['EmuB1', 'EmuB2', 'EmuB3', 'EmuB4']
bandxs = [addplots(lo[k], addplots(nlo[k], nnlo[k])) for k in bands]
bandk1 = [sanitise(divideplots(nlo[k], lo[k])) for k in bands]
bandk2 = [
    sanitise(divideplots(nnlo[k], addplots(lo[k], nlo[k])))
    for k in bands
]
fig, (ax1, ax2, ax3) = threepanel(
    labelx="$E_\\mu^{\\rm kin}\,/\,{\\rm MeV}$",
    upleft=bandxs,
    labupleft="$\\D\\sigma/\\D E_\\mu^{\\rm kin}\,/\,{\\rm \\upmu b}$",
    middleleft=bandk1, labmiddleleft="$\\delta K^{(1)}$",
    downleft=bandk2, labdownleft="$\\delta K^{(2)}$",
    colupleft=[orderscheme['nnlo'], 'paleturquoise', 'olivedrab', 'gold'],
    colmiddleleft=[orderscheme['nnlo'], 'paleturquoise', 'olivedrab', 'gold'],
    coldownleft=[orderscheme['nnlo'], 'paleturquoise', 'olivedrab', 'gold']
)
ax1.set_yscale('log')
ax2.set_ylim(0.982-1, 1.016-1)
ax3.set_ylim(0.999-1, 1.018-1)

thbounds = np.array([
    [
        0.3875697,  # about 22 degrees -> 126 MeV
        0.7708983   # about 44 degrees -> 117 MeV
    ], [
        0.8054385,  # about 46 degrees -> 116 MeV
        1.0939359   # about 63 degrees -> 107 MeV
    ], [
        1.124739,   # about 64 degrees -> 106 MeV
        1.403278    # about 80 degrees ->  97 MeV
    ], [
        1.435036,   # about 82 degrees ->  96 MeV
        1.739436    # about 99 degrees ->  87 MeV
    ]
])
ax2.legend([
    "$%4.1f^\\circ \\le \\theta_\\mu \\le %4.1f^\\circ$" % tuple(i)
    for i in thbounds/pi * 180
])

mulify(fig)

fig.savefig('plots/bands.pdf')
###########################################################}}}
#####################################################################}}}
