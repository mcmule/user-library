# Michel decays $`L\to \nu\bar\nu l`$

References: [[1909.10244]](https://arxiv.org/abs/1909.10244), [[1811.06461]](https://arxiv.org/abs/1811.06461), MSc thesis of A. Gurgone

Configurations implemented

 * No cuts to obtain decay rate
   [[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/validation/validation.ipynb?job=run)
 * Demonstration cuts: no photons with $`E_\gamma > 10\,{\rm MeV}`$
   within a cone of $`|\cos\sphericalangle(\vec p_\gamma,\vec p_e)| > 0.8`$
   [[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/demo-observable/demo-observable.ipynb?job=run)
 * MEG cuts with photon veto
 * Full reconstruction of
   [[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/f-and-g/f-and-g.ipynb?job=run)
```math
\frac{{\rm d}^2\Gamma}{{\rm d}E_e\,{\rm d}(\cos\theta)} =
\Gamma_0(f(E_e)+P\,\cos\theta\ g(E_e))
```
