# Muon electron scattering $`\mu e\to \mu e`$

References: [[tbd]](https://arxiv.org/abs/20??.?????)

Configurations implemented
 * MUonE experiment, Setups 2 and 4
   [[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/muone/muone.ipynb?job=run)

