# Example calculation

This is the example calculation used in the McMule paper. It mostly
serves to *prove* the discrepancy between the experimental measurement
of $`\tau\to\nu\bar\nu e\gamma`$ in
[[1502.01784]](https://arxiv.org/pdf/1502.01784.pdf) and the NLO results of
[[1506.03416]](https://arxiv.org/pdf/1506.03416.pdf) and McMule
[[1705.03782]](https://arxiv.org/pdf/1705.03782.pdf).

We require $`E_\gamma > 10\,{\rm MeV}`$ in the $`\tau`$ restframe.
[[ref]](https://gitlab.psi.ch/mcmule/user-library/-/jobs/artifacts/master/file/ci/example/example.ipynb?job=run)
