# vim: foldmethod=marker
## Initialise pyMule{{{
from pymule import *

# To normalise branching ratios, we need the $\tau$ lifetime
lifetime = 1/(1000*(6.582119e-25)/(2.903e-13))
#####################################################################}}}
## Load data{{{
# The folder where McMule has stored the statefiles
setup(folder='babar-tau-e/out.tar.bz2')

# Import LO data and re-scale to branching ratio
LO = scaleset(mergefks(sigma('m2enng0')), GF**2*lifetime*alpha)

# Import NLO corrections from the three pieces
# $\mathcal{M}_{n+1}^{(0)}$, $\mathcal{M}_{n}^{(1)}$ and $\hat{\mathcal{E}}$
NLO = scaleset(mergefks(
    sigma('m2enngR'),       # real corrections
    sigma('m2enngCT'),      # counter term
    anyxi=sigma('m2enngV')  # virtual corrections
), GF**2 * lifetime * alpha**2)
#####################################################################}}}
## Results{{{
### Branching ratio{{{
# The branching ratio $\mathcal{B}_{\rm NLO} = \mathcal{B}_{\rm LO} +
# \delta\mathcal{B}$
fullNLO = plusnumbers(LO['value'], NLO['value'])

# Print results
print("BR_0 = " + printnumber(LO['value']))
print("dBR  = " + printnumber(NLO['value']))
print("BR_1 = " + printnumber(fullNLO))
###########################################################}}}
### $E_e$ plot{{{
fig1, (ax1, ax2) = kplot(
    {'lo': LO['Ee'], 'nlo': NLO['Ee']},
    labelx=r"$E_e\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D E_e$"
)
ax2.set_ylim(0.8, 1.01)
savefig("ee.pdf")

###########################################################}}}
### $m_{e\gamma}$ mass{{{
fig2, (ax1, ax2) = kplot(
    {'lo': LO['minv'], 'nlo': NLO['minv']},
    labelx=r"$m_{e\gamma}\,/\,{\rm MeV}$",
    labelsigma=r"$\D\mathcal{B}/\D m_{e\gamma}$"
)

ax1.set_yscale('log')
ax1.set_xlim(1000, 0)
ax1.set_ylim(5e-9, 1e-3)
ax2.set_ylim(0.8, 1.)

savefig("meg.pdf")
###########################################################}}}
#####################################################################}}}
